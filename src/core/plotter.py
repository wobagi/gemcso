
import matplotlib.pyplot as plt
import numpy as np 
import matplotlib.cm as cm
from mpl_toolkits.basemap import Basemap

class CSO_Plotter:
	'''
	This class collects some usefull plotting functions
	'''
	def __init__(self,output_directory):
		self.plotting_dir = output_directory
		self.figsize = (25,25)
		#setting global plotter settings
		plt.style.use('ggplot')

	def plot_on_footprint(self,csf, array=None, output_file=None,plot_title=None,region='PL'):
		'''
		This function plots on a Sentinel footprint
		csf is a CSO_S5p_File object with footprint
		If no field is given, vcd from csf is ploted
		'''
		plt.figure(figsize=self.figsize)
		if region=='PL':
			m = Basemap(width=1000000,height=1000000,
			resolution='h',projection='lcc', lat_0 = 52, lon_0 = 20)

			m.drawcoastlines()
			m.drawcountries()
			m.drawparallels(np.arange(48.,56.,2.),labels=[False,True,True,False])
			m.drawmeridians(np.arange(14.,28.,2.),labels=[True,False,False,True])
		elif region=='CAMS':
			m = Basemap(width=5000000,height=5000000,
			resolution='h',projection='lcc', lat_0 = 53, lon_0 = 5)

			m.drawcoastlines()
			m.drawcountries()
			m.drawparallels(np.arange(30.,75.,5.),labels=[False,True,True,False])
			m.drawmeridians(np.arange(-30.,45.,5.),labels=[True,False,False,True])
					
		if array is None:
			array = csf.vcd

		cmax = np.max([np.nanmean(array)+2*np.nanstd(array),1e-4)
		m.scatter(
			x=csf.lons,
			y=csf.lats,
			s=3,
			c=array,
			cmap="jet",
			marker="o",
			alpha=1.0,
			latlon =True,
			vmin=0,
			vmax=cmax
		)
		plt.title(plot_title)
		plt.grid(True)
		cbar = plt.colorbar()
		cbar.ax.tick_params(labelsize=14)
		cbar.ax.set_ylabel('$mol / m^2$', rotation=270)

		if output_file is None:
			plt.show()
		else:
			plt.savefig(self.plotting_dir +output_file,bbox_inches="tight")
			plt.close()

	def compare_profiles(self,profile1,profile2,
		output_file=None,title1=None,title2=None,
		invert_yaxis=None): 
		'''
		This function compares two profiles from different models
		values are ploted against layer_number (not altitude!)
		'''
		fig, axs = plt.subplots(1,2,figsize=self.figsize)
		axs[0].plot(profile1,np.arange(profile1.shape[0]),'x-')
		axs[1].plot(profile2,np.arange(profile2.shape[0]),'x-')
		axs[0].set_title(title1)
		axs[1].set_title(title2)
		
		#In case layer 0 is the top one (like in gemaq)
		if invert_yaxis == 'left':
			axs[0].invert_yaxis()
		elif invert_yaxis == 'right':
			axs[1].invert_yaxis()
		elif invert_yaxis == 'both':
			axs[0].invert_yaxis()
			axs[1].invert_yaxis()

		if output_file is None:
			plt.show()
		else:
			plt.savefig(self.plotting_dir +output_file)
			plt.close()
	
	def compare_profiles_px(self,profile1,profile2,
		px_profile1,px_profile2,logaxis=None,
		output_file=None,title=None,legend=[],invert_yaxis=False): 
		'''
		This function compares two profiles from different models
		values are ploted on the same plot, agains pressure level
		'''
		plt.figure(figsize=self.figsize)
		plt.plot(profile1,px_profile1,'x-')
		plt.plot(profile2,px_profile2,'x-')
		plt.title(title)
		plt.legend(legend)
		
		if logaxis=='y':
			plt.gca().set_yscale('log')
		elif logaxis=='x':
			plt.gca().set_xscale('log')
		elif logaxis=='both':
			plt.gca().set_yscale('log')
			plt.gca().set_xscale('log')
		
		if invert_yaxis is True:
			plt.gca().invert_yaxis()
		if output_file is None:
			plt.show()
		else:
			plt.savefig(self.plotting_dir +output_file)
			plt.close()