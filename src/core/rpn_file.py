from rpn.rpn_multi import MultiRPN
from rpn.rpn import RPN    
import numpy as np
import os
from pathlib import Path
from datetime import date,timedelta,datetime
from core.constants import BOLTZ

class CSO_RPN_File:
    def __init__(self, gem_output_dir,dt0=None,h_start=11,h_end=13):
        if dt0 is None:
            dt0 =datetime(2019, 4, 1, 9, 0)
        self.data= dt0.strftime('%Y%m%d')
        self.h_start = h_start
        self.h_end = h_end
        #interesują nas jedynie godziny 9-11, gdyż wtedy lata Sentinel-5P a 11-13 robi najlepsze zdjęcia
        
        #Poland
        #self.dm_path_list =[str(gem_output_dir / f"{self.data}00/dm{self.data}00-00-00_000000{i:02d}h") for i in range(9,12)]
        #self.km_path_list =[str(gem_output_dir / f"{self.data}00/km{self.data}00-00-00_000000{i:02d}h") for i in range(9,12)]
        
        #CAMS hourly results
        #self.dm_path_list =[str(gem_output_dir / f"{self.data}00/dm{self.data}00-00-00_{i:03d}h") for i in range(9,16)]
        #self.km_path_list =[str(gem_output_dir / f"{self.data}00/km{self.data}00-00-00_{i:03d}h") for i in range(9,16)]

        #CAMS every 450s
        self.dm_path_list =[str(gem_output_dir / f"{self.data}00/dm{self.data}00-00-00_{i*3600:06d}s") for i in range(h_start,h_end)]
        self.km_path_list =[str(gem_output_dir / f"{self.data}00/km{self.data}00-00-00_{i*3600:06d}s") for i in range(h_start,h_end)]

        rpn_src = RPN(self.dm_path_list[0])
        rpn_array = np.array(rpn_src.variables['GZ'][0][0][0])
        lons2d, lats2d = rpn_src.get_longitudes_and_latitudes_for_the_last_read_rec()
        rpn_src.close()

        #setting up lons and lats
        self.lons = lons2d.flatten()
        self.lats = lats2d.flatten()

    def get_levels(self):
        '''
        Function returns levels (assuming all days are identical in 4D arrays)
        '''
        key0 = next(iter(self.gz.keys()))
        return list(self.gz[key0].keys())

    def get_timesteps(self):
        '''
        Function returns datetimes (first-level keys in 4D arrays)
        '''
        return list(self.gz.keys())


    def load_4D_fields(self):
        r_dm = MultiRPN(self.dm_path_list)
        r_km = MultiRPN(self.km_path_list)
        
        #These variables (GZ,TT,PX,NO2) are the default ones used by CSO
        self.gz = r_dm.get_4d_field(varname='GZ')
        self.tt = r_dm.get_4d_field(varname='TT')
        self.px = r_dm.get_4d_field(varname='PX')
        self.no2 = r_km.get_4d_field(varname='NO2')
        
        r_dm.close()
        r_km.close()

    def select_timestep(self,ts1):
        self.selected_timestep = ts1
        self.gz_step = self.gz[ts1]
        self.tt_step = self.tt[ts1]
        self.px_step = self.px[ts1]
        self.no2_step = self.no2[ts1]

    def calculate_tnd_profile(self):
        if self.selected_timestep is None:
            raise Exception("no timestep selected")
        
        self.tnd_step = dict()
        src_levels = self.get_levels()

        for lvl in src_levels:
            self.tnd_step[lvl]=1e-6*(self.px_step[lvl]*100)/(BOLTZ*(self.tt_step[lvl]+273.15))


    def add_px2rpns(self, temp_dir=None, clear_old_junk=True):
        '''
        function which adds px to rpn files, using r.hy2press
        '''
        
        for file in self.dm_path_list:
            #desired filename
            new_filename = Path(file).stem + '_tmp.rpn'
            temporary_path = Path(temp_dir) / new_filename

            #removing the old one (if exists)
            if temporary_path.exists() and clear_old_junk:
                temporary_path.unlink()

            #generating PX layers
            cmd = f"r.hy2pres -iment {file} -ozsrt {str(temporary_path)} -var TT >/dev/null"
            os.system(cmd)
                        
            #Adding other fields to rpn
            cmd2 = f"editfst -s {file} -d {str(temporary_path)} -i <<`cat fst.cmd` 1>/dev/null 2>/dev/null"
            os.system(cmd2)

            #cmd2 is expected to look like this:
            #cmd2 = ' editfst -s dm2020011900-00-00_00000000h -d junk/'+new_file+' -i <<`cat fst.cmd` >/dev/null'
            #print(cmd2)

        #updating dm list pointer
        self.dm_path_list = [str(Path(temp_dir)) + '/dm' + self.data +
            '00-00-00_' + str(i*3600).zfill(6) + 's_tmp.rpn'
            for i in range(self.h_start, self.h_end)]
        '''
        self.dm_path_list = [str(Path(temp_dir)) + '/dm' + self.data +
            '00-00-00_000000' + str(i).zfill(2) + 'h_tmp.rpn'
            for i in range(9, 12)]
        '''