from scipy.interpolate import griddata
from scipy.interpolate import interp1d
import numpy as np 

class CSO_Converter:
    '''
    Class for converting model grid to S5p footprint
    '''
    def __init__(self,cso_s5p_file,cso_rpn_file):
        self.crpn = cso_rpn_file
        self.csf = cso_s5p_file

    def perform_scipy_interpolation(self,src_array,method='nearest'):
        '''
        This is quick and dirty interpolation using scipy and two pointclouds
        '''
        #fix for Greenwich meridian
        src_lons = self.crpn.lons
        src_lons[src_lons>180]=src_lons[src_lons>180]-360

        rpn_points = np.vstack([src_lons,self.crpn.lats]).T
        csf_points = np.vstack([self.csf.lons, self.csf.lats]).T

        csf_values = griddata(rpn_points , src_array, csf_points, method=method)

        return csf_values
    
    def scipy_interpolate_column(self,src_column_array):
        '''
        This function provides horizontal interpolation at all levels
        '''
        src_levels = self.crpn.get_levels()
        nlevels = len(src_levels)
        npixels = self.csf.get_pixel_number()

        #space for interpolated column
        new_csf_cloumn = np.empty([npixels,nlevels])

        ilvl=0
        for lvl in src_levels:
            src_array_2D = np.array(src_column_array[lvl]).flatten()

            new_csf_cloumn[:,ilvl] = self.perform_scipy_interpolation(src_array_2D)
            ilvl=ilvl+1
        
        return new_csf_cloumn

    def scipy_interpolate_verticaly(self,tm5_px_column,gem_px_column,gem_C_cloumn):
        '''
        This function performs vertical interpolation using pressure levels
        Usual run is C(28 gem_levels) -> C(34 TM5 levels)
        
        Note: I am not sure if both models have conc and px given at the same level
        '''
        #Both C arrays are assumed to be on the same footprint

        tm5_C_column = np.empty_like(tm5_px_column)
        #iteration over pixels
        for i in range(self.csf.get_pixel_number()):
            #first let's extract profiles at i-pixel
            px_TM5 = tm5_px_column[i,:]
            px_GEM = gem_px_column[i,:]
            C_GEM  = gem_C_cloumn[i,:]

            #building interpolator
            f1 = interp1d(px_GEM, C_GEM, kind='linear', fill_value='extrapolate')
            tm5_C_column[i,:] = f1(px_TM5)

        return tm5_C_column