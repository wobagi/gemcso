
import xarray as xr 
import matplotlib.pyplot as plt
import numpy as np 
import matplotlib.cm as cm
from mpl_toolkits.basemap import Basemap

class CSO_S5p_File:
    def __init__(self,s5p_file, mode='r'):
        if mode == 'r':
            try:
                self.file_name = s5p_file
                self.xarr = xr.open_dataset(s5p_file)
                self.lons = self.xarr.longitude.values
                self.lats = self.xarr.latitude.values
                self.vcd = np.squeeze(self.xarr.vcd.T.values)
                self.pixel = self.xarr.pixel.values
                self.pressure = self.xarr.pressure.values
                self.kernel_trop = np.squeeze(self.xarr.kernel_trop.values)
                #Airmass factor
                self.amf = np.squeeze(self.xarr.amf)
                self.amf_trop = np.squeeze(self.xarr.amf_trop)
                #support data for masking
                self.qa_value = self.xarr.qa_value
                self.cloud_fraction = self.xarr.cloud_fraction
                
                #TM5 layer index of the highest layer in the tropopause
                self.nla = np.squeeze(self.xarr.nla.values)
                
            except FileNotFoundError:
                #logger.error("File "+s5p_file+" not found!")
                raise

        elif mode=='w':
            self.file_name = s5p_file
            self.xarr = xr.Dataset()
        else:
            #logger.error("Unknown access mode")
            raise

    def save_dataset(self):
        self.xarr.to_netcdf(self.file_name)

    #############################
    # set methods
    #############################
    
    def set_coordinates(self,coordinates):
        self.xarr = self.xarr.assign_coords(coordinates)
    
    def set_longname(self,variable,longname):
        self.xarr[variable] = self.xarr[variable].assign_attrs({'long_name':longname})
    
    def set_units(self,variable,units):
        self.xarr[variable] = self.xarr[variable].assign_attrs({'units':units})
    
    
    #############################
    # get methods
    #############################

    def get_coordinates(self):
        return self.xarr.coords
    
    def get_pixel_number(self):
        return self.xarr.pixel.values.shape[0]

    def get_mean_pressure_profile(self):
        '''
        Function returns averaged pressure profile on the whole domain
        '''
        return np.mean(self.pressure,axis=0)

    #############################
    # add methods
    #############################
    
    def add_variable(self, varname, coordinates, array):
        '''
        Function adds a variable (varname) to self.xarr Dataset,
        coordinates assumed to be a tuple
        '''
        self.xarr[varname] = ((coordinates),array)
        
    #############################
    #   masking methods
    #############################
    def mask_poor_quality(self,array,qa_treshold=0.7):
        '''
        function masks array values where qa is below the given treshold
        '''
        poor_qa_mask = (self.qa_value<qa_treshold)
        array[poor_qa_mask] = np.nan
        return array

    def mask_clouds(self,array,cc_treshold=0.9):
        '''
        function masks array values where cloud cover is above the given treshold
        '''
        cloud_mask = (self.cloud_fraction>cc_treshold)
        array[cloud_mask] = np.nan
        return array