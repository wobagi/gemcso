import numpy as np

#Steven-Boltzman constant
BOLTZ = 1.3806505e-23

GAS_CONSTANT = 8.31446261815324	 # ideal gas constant (J / K⋅mol)
RD_DRY_AIR = 287.058  # specific gas constant for dry air (J / kg⋅K)
RD_VAPOR = 461.495  # specific gas constant for water vapor (J / kg⋅K)
MM_DRY_AIR = 28.9652  # molecular mass of dry air

EARTH_RADIUS = 6371009  # mean earth radius (m)

PI = np.pi  # radians
TAU = 2 * PI  # radians
DEG2RAD = PI/180.0  # radians
RAD2DEG = 180.0/PI  # degrees
