#!/usr/bin/env python3

import os
import sys
import logging
import argparse
import numpy as np

from pathlib import Path
from datetime import date, timedelta, datetime
from rpn.rpn import RPN    
from rpn.rpn_multi import MultiRPN

from core.s5p_file import CSO_S5p_File
from core.rpn_file import CSO_RPN_File
from core.converter import CSO_Converter
from core.constants import BOLTZ


TEST_SENSOR_PATH = "/home/mkawka/pythony/cso_gem/CSO-data/S5p/RPRO/NO2/PL/2019/04/S5p_RPRO_NO2_07591.nc"
TEST_MODEL_PATH = '/mnt/disk2/jacek/prognoza_s5p/local/'


def run(args):
    sensor_data = args.sat
    model_data = args.model
    output_dir = args.output

    plot_dir = output_dir / "plot"
    if not plot_dir.is_dir():
        plot_dir.mkdir(parents=True)

    temp_dir = output_dir / "junk"
    if not temp_dir.is_dir():
        temp_dir.mkdir(parents=True)

    ts1 = datetime(2019, 4, 1, 9, 0)

    csf = CSO_S5p_File(sensor_data)

    crpn = CSO_RPN_File(model_data)
    crpn.add_px2rpns(temp_dir=temp_dir)
    crpn.load_4D_fields()
    crpn.select_timestep(ts1)

    lvl = 1.0
    src_array_no2 = np.array(crpn.no2_step[lvl]).flatten()
    converter = CSO_Converter(cso_s5p_file=csf, cso_rpn_file=crpn)
    new_csf_field = converter.perform_scipy_interpolation(src_array_no2)

    csf.plot(output_file=(plot_dir / "base_vcd.png"))
    csf.plot(array=new_csf_field, output_file=(plot_dir / "interpolated_NO2_from_RPN.png"))


def main():
    parser = argparse.ArgumentParser(description="CSO with GEM data")
    parser.add_argument(
        "--sat", "-s",
        #required=True,
        type=Path,
        default=TEST_SENSOR_PATH,
        help="Sensor data file/dir path"
    )
    parser.add_argument(
        "--model", "-m",
        #required=True,
        type=Path,
        default=TEST_MODEL_PATH,
        help="Model data file/dir path"
    )
    parser.add_argument(
        "--output", "-o",
        type=Path,
        default=os.getcwd(),
        help="Output directory. Defaults to current working dir."
    )
    args = parser.parse_args()
    run(args)


if __name__=="__main__":
    main()

