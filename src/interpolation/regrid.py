"""
Regridding methods
"""

import numpy as np

from dataclasses import dataclass
from typing import Protocol


class InterpolationMethod(Protocol):
    def __call__(self, *args, **kwargs):
        ...

@dataclass
class WeightedAreaInterpolation():
    """
    Interpolation method based on weighted averages of overlapping areas
    """
    precision: int = 0

    def __call__(self, data, source_grid, target_grid):
        ...