
#!/usr/bin/env python3

import os
import argparse
import numpy as np

from pathlib import Path
from datetime import date, timedelta, datetime
from rpn.rpn import RPN    
from rpn.rpn_multi import MultiRPN

from core.s5p_file import CSO_S5p_File
from core.rpn_file import CSO_RPN_File
from core.converter import CSO_Converter
from core.plotter import CSO_Plotter
from core.constants import BOLTZ
from scipy.interpolate import interp1d

import matplotlib.pyplot as plt
import pprint as pp 
from pathlib import Path

import pandas as pd
import logging
import sys

from core.listing import CSO_ListingProcessor

'''
Próba ostatecznego rozwiązania dla zadanego okresu
'''

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)


#Paths settings
listing_file = '/mnt/fast01/cso/CSO-data/S5p/listing-NO2-CAMS.csv'
gem_output_dir = Path('/mnt/disk4/cams50pl/c19/c02/gv15/out/')
sensor_prefix= Path('/mnt/fast01/cso/CSO-data/S5p/')
output_prefix= Path('/mnt/fast01/cso/GEMAQ-oper/')
png_dir = Path('/home/mkawka/wymiana/cso/png/')
tmp_dir ='./junk/'
day=1
dt0 =datetime(2018, 6, day,1, 0)
h_start=3
h_end=15

lsp = CSO_ListingProcessor(listing_file)
start_date=datetime(2018,6,day,h_start,0)
end_date=datetime(2018,6,day,h_end,0)

#to zwraca zobrazowania z przedziału
res = lsp.get_filelist(start_date,end_date)
#odtąd pętla po kluczach z lsp

logging.info('Starting objects initialization')
ploter = CSO_Plotter(png_dir)
crpn = CSO_RPN_File(gem_output_dir,dt0,h_start,h_end)

crpn.add_px2rpns(temp_dir=tmp_dir)
crpn.load_4D_fields()

logging.info('Starting iteration over timesteps')
for ts_s5p in list(res.keys()):
        csf = CSO_S5p_File(str(sensor_prefix)+'/'+res[ts_s5p])
        converter = CSO_Converter(cso_s5p_file=csf, cso_rpn_file=crpn)
        
        #looking for gem-aq timestep, closest to ts0:
        ts_rpn = min(crpn.get_timesteps(), key=lambda x: abs(x - ts_s5p))
        ts_rpn_str = ts_rpn.strftime("%Y%m%d_%H%M%S")
        ts_s5p_str = ts_s5p.strftime("%Y%m%d_%H%M%S")

        logging.info("Processing S5P ts "+ts_s5p_str+" with GEMAQ ts "+ts_rpn_str)

        crpn.select_timestep(ts_rpn)
        crpn.calculate_tnd_profile()

        #pozioma interpolacja do footprintu
        gem_px_column = converter.scipy_interpolate_column(src_column_array=crpn.px_step)*100 #hPa -> Pa
        tm5_px_column = csf.pressure

        #wykresy poziomów ciśnienia vs. numer warstw
        #ploter = CSO_Plotter('/home/mkawka/pythony/cso_gem/png_output/')
        #ploter.compare_profiles(np.mean(gem_px_column,axis=0),np.mean(tm5_px_column,axis=0),
        #        title1='gem-aq PX',title2='TM5 PX',
        #        invert_yaxis='left',output_file='PX_comparison.png')

        logging.info('\tStarting interpolation ')
        #interpolacja pozioma do footprintu
        new_tnd_column = converter.scipy_interpolate_column(src_column_array=crpn.tnd_step)        
        new_no2_column = converter.scipy_interpolate_column(src_column_array=crpn.no2_step)        
        new_gz_column = converter.scipy_interpolate_column(src_column_array=crpn.gz_step)        


        #interpolacja pionowa do warstw TM5
        tm5_tnd_column = converter.scipy_interpolate_verticaly(tm5_px_column,gem_px_column,new_tnd_column)
        tm5_no2_column = converter.scipy_interpolate_verticaly(tm5_px_column,gem_px_column,new_no2_column)
        tm5_gz_column = converter.scipy_interpolate_verticaly(tm5_px_column,gem_px_column,new_gz_column)
        logging.info('\tFinished interpolation ')

        #kolumna wg. 34 poziomów TM5
        dGZ = np.diff(tm5_gz_column,axis=1)*9.81*100 #wymiar (17913, 34)
        tmp = tm5_tnd_column*tm5_no2_column
        cno2  = np.empty_like(tm5_gz_column) 
        cno2_AVK = np.empty_like(tm5_gz_column) 

        for i in range(dGZ.shape[1]): 
        #tu uzupełniamy od 1 do 34, 
        # poziom 0 jako przyziemny liczymy oddzielnie
                cno2[:,i+1] = dGZ[:,i] * (tm5_tnd_column[:,i+1]*tm5_no2_column[:,i+1] 
                        +tm5_tnd_column[:,i]*tm5_no2_column[:,i])/2

        #warstwa przypowierzchniowa
        cno2[:,0] = tm5_gz_column[:,0] * tm5_tnd_column[:,0]*tm5_no2_column[:,0]
        #kernel ma 34 poziomy, a cno2 35, rozszerzamy o 1
        new_kernel = np.insert(csf.kernel_trop,-1,values=0,axis=1)
        cno2_AVK = cno2 * new_kernel

        
        #column sum and unit conversion molec/cm2 -> mol/m2
        CNO2_AVK = np.sum(cno2_AVK,axis=1) / (6.022*1e19)
        CNO2_total = np.sum(cno2,axis=1) / (6.022*1e19)


        csf.vcd = csf.mask_poor_quality(csf.vcd)
        CNO2_AVK = csf.mask_poor_quality(CNO2_AVK)
        
        #Sx - partial (tropospheric) column from a profile up to nla layer
        CNO2_trop = np.empty_like(CNO2_total)
        for i in range(cno2.shape[0]): 
                CNO2_trop[i] = np.sum(cno2[i,0:csf.nla[i]])/ (6.022*1e19)

        #M_m - air mass factor from local model
        M_m = (csf.amf_trop * CNO2_AVK)/CNO2_trop

        #A_m - averaging kernel using local airmass factors
        A_m = np.empty_like(csf.kernel_trop)
        for i in range(A_m.shape[1]):
                A_m[:,i] = ( csf.amf_trop / M_m ) * csf.kernel_trop[:,i]
        
        #ys_m - simulated retrieval based on local AMF
        new_kernel = np.insert(A_m,-1,values=0,axis=1)
        ys_m = cno2 * new_kernel
        YS_m = np.sum(ys_m,axis=1) / (6.022*1e19)
        YS_m = csf.mask_poor_quality(YS_m)

        #yr_m - alt retrieval based on local AMF
        YR_m = ( csf.amf_trop / M_m ) * csf.vcd

        '''              
        logging.info('\tGenerating PNG plots...')
        ploter.plot_on_footprint(csf,csf.xarr.qa_value,output_file='QA_'+ts_s5p_str+'.png',plot_title='QA',region='CAMS')
        ploter.plot_on_footprint(csf,csf.xarr.cloud_fraction,output_file='CF_'+ts_s5p_str+'.png',plot_title='cloud_fraction',region='CAMS')
        ploter.plot_on_footprint(csf,csf.vcd,output_file='VCD_'+ts_s5p_str+'.png',plot_title='S5P_VCD',region='CAMS')
        ploter.plot_on_footprint(csf,YR_m,output_file='yr_m_'+ts_s5p_str+'.png',plot_title='S5P_VCD based on local AMF',region='CAMS')
        ploter.plot_on_footprint(csf,CNO2_AVK,output_file='CNO2_AVK_'+ts_rpn_str+'.png',plot_title='CNO2_AVK',region='CAMS')
        ploter.plot_on_footprint(csf,CNO2_trop,output_file='CNO2_trop_'+ts_rpn_str+'.png',plot_title='CNO2_trop',region='CAMS')
        ploter.plot_on_footprint(csf,M_m,output_file='M_m_'+ts_rpn_str+'.png',plot_title='air mass factor from local model',region='CAMS')
        ploter.plot_on_footprint(csf,csf.amf_trop,output_file='M_'+ts_rpn_str+'.png',plot_title='air mass factor from TM5',region='CAMS')
        logging.info('\tFinished generating PNG plots ')
        '''

        #Saving nc for further processing
        state_file = "CSO_output_"+ts_rpn.strftime("%Y%m%d")+'_'+ts_rpn.strftime("%H%M")+"_state.nc"       
        logging.info('\tSaving results to '+state_file)
        csf_state = CSO_S5p_File(output_prefix / state_file ,mode='w')
        csf_state.set_coordinates(csf.get_coordinates())
        
        #1D
        csf_state.add_variable('M_m',('pixel'),M_m)
        csf_state.add_variable('Sx',('pixel'),CNO2_trop)
        csf_state.add_variable('ys',('pixel'),CNO2_AVK)
        csf_state.add_variable('ys_m',('pixel'),YS_m)
        csf_state.add_variable('yr_m',('pixel'),YR_m)
        
        #2D
        csf_state.add_variable('A_m',('pixel','layer'),A_m)
        csf_state.add_variable('xs',('pixel','layer'),cno2[:,:-1])

        #pozostało jeszcze mod_cc, mod_conc, mod_hp, mod_tcc
        
        #Attributes
        csf_state.set_longname('M_m',"airmass factors from local model")
        csf_state.set_longname('A_m',"averaging kernel using local airmass factors")
        csf_state.set_longname('xs',"model simulations at apriori layers")
        csf_state.set_longname('Sx',"tropospheric column in local model")
        csf_state.set_longname('yr_m',"retrieval using local airmass factors")
        csf_state.set_longname('ys_m',"simulated retrieval based on local airmass factors")
        csf_state.set_longname('ys',"simulated retrieval")
        csf_state.set_units('A_m',"1")
        csf_state.set_units('M_m',"1")
        csf_state.set_units('xs',"mol m-2")
        csf_state.set_units('Sx',"mol m-2")
        csf_state.set_units('yr_m',"mol m-2")
        csf_state.set_units('ys_m',"mol m-2")
        csf_state.set_units('ys',"mol m-2")
        
        csf_state.save_dataset()

        data_file = "CSO_output_"+ts_rpn.strftime("%Y%m%d")+'_'+ts_rpn.strftime("%H%M")+"_data.nc"
        logging.info('\tSaving results to '+data_file)
        csf_data = CSO_S5p_File(output_prefix / data_file ,mode='w')
        csf_data.set_coordinates(csf.get_coordinates())

        #1D
        csf_data.add_variable('M',('pixel'),csf.amf_trop)
        csf_data.add_variable('yr',('pixel'),csf.vcd)

        #2D
        csf_data.add_variable('A',('pixel','layer'),csf.kernel_trop)
        csf_data.add_variable('track_longitude_bounds',('track_scan', 'track_pixel', 'corner'),csf.xarr.track_longitude_bounds)
        csf_data.add_variable('track_latitude_bounds',('track_scan', 'track_pixel', 'corner'),csf.xarr.track_latitude_bounds)
        

        #Attributes
        csf_data.set_units('A',"1")
        csf_data.set_units('M',"1")
        csf_data.set_units('yr',"mol m-2")
        csf_data.set_units('track_latitude_bounds',"degrees_north")
        csf_data.set_units('track_longitude_bounds',"degrees_east")
        csf_data.set_units('pixel',"1")
        
        csf_data.save_dataset()

        logging.info('Finished processing timestep')